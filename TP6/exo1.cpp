#include "tp6.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;

void Graph::buildFromAdjenciesMatrix(int **adjacencies, int nodeCount)
{
	/**
	  * Make a graph from a matrix
	  * first create all nodes, add it to the graph then connect them
	  * this->appendNewNode
	  * this->nodes[i]->appendNewEdge
	  */
	for(int i=0; i<nodeCount; i++)
	{
        this->appendNewNode(new GraphNode(i));
    }

    for(int i=0; i<nodeCount; i++)
	{
        for(int j=0; j<nodeCount; j++)
		{
            if(adjacencies[i][j] != 0)
			{
                this->nodes[i]->appendNewEdge(this->nodes[j],adjacencies[i][j]);
            }
        }
    }
}

void Graph::deepTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	  * Fill nodes array by travelling graph starting from first and using recursivity
	  */
	nodes[nodesSize] = first;
    nodesSize++;
    visited[first->value] = true;

    Edge * temporary = first->edges;

    while(temporary != NULL)
	{
        if(!visited[temporary->destination->value])
		{
            deepTravel(temporary->destination, nodes, nodesSize, visited);
        }
        temporary = temporary->next;
    }

}

void Graph::wideTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	 * Fill nodes array by travelling graph starting from first and using queue
	 * nodeQueue.push(a_node)
	 * nodeQueue.front() -> first node of the queue
	 * nodeQueue.pop() -> remove first node of the queue
	 * nodeQueue.size() -> size of the queue
	 */
	std::queue<GraphNode*> nodeQueue;
	nodeQueue.push(first);

	while(!nodeQueue.empty())
	{
        GraphNode * noeudActuel = nodeQueue.front();
        nodeQueue.pop();
        nodes[nodesSize] = noeudActuel;
        nodesSize++;
        visited[noeudActuel->value] = true;

        Edge * temporary = noeudActuel->edges;

        while(temporary != NULL)
		{
            if(!visited[temporary->destination->value]){
                nodeQueue.push(temporary->destination);
            }
            temporary = temporary->next;
        }
    }
}

bool Graph::detectCycle(GraphNode *first, bool visited[])
{
	/**
	  Detect if there is cycle when starting from first
	  (the first may not be in the cycle)
	  Think about what's happen when you get an already visited node
	**/
    bool isCycle = false;
    std::queue<GraphNode*> nodeQueue;
    nodeQueue.push(first);


    while(!nodeQueue.empty() && !isCycle)
	{
        GraphNode * current = nodeQueue.front();
        nodeQueue.pop();
        visited[current->value] = true;

        Edge * temporary = current->edges;

        while(temporary != NULL && !isCycle)
		{
            if(temporary->destination == first)
			{
                isCycle = true;
            }
			else
			{
                if(!visited[temporary->destination->value])
				{
                    nodeQueue.push(temporary->destination);
                }
            }
            temporary = temporary->next;
        }
    }

    return isCycle;
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 150;
	w = new GraphWindow();
	w->show();

	return a.exec();
}

#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
	if (origin.size()<2)
	{
		if(origin.size() == 2 && origin[1] < origin[0])
		{
			int tmp = origin[0];
			origin[0] = origin[1];
			origin[1] = tmp;
		}

		return ;
	}
	// initialisation
	Array& first = w->newArray(origin.size()/2);
	Array& second = w->newArray(origin.size()-first.size());
	
	// split

	int halfSize = origin.size()/2;

	for (int i = 0; i< halfSize ; i++){
		first[i] = origin[i];
	}


	for (int j = halfSize ; j < origin.size(); j++) {
		second[j- halfSize] = origin[j];
	}

	// recursiv splitAndMerge of lowerArray and greaterArray

	splitAndMerge(first);
	splitAndMerge(second);

	// merge

	merge(first,second,origin);
}

void merge(Array& first, Array& second, Array& result)
{
	int i=0, j=0;

	for(int indice=0; indice<result.size(); indice++)
	{
		if(i == first.size())
		{
			result[indice] = second[j];
			j++;
		}
		else if(j == second.size())
		{
			result[indice] = first[i];
			i++;
		}
		else if(first[i] < second[j])
		{
			result[indice] = first[i];
			i++;
		}
		else{
			result[indice] = second[j];
			j++;
		}
	}
}




void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}

#include <QApplication>
#include <time.h>

#include "tp2.h"


MainWindow* w=nullptr;

Array fusion(Array& tab1, Array& tab2, int size1 , int size2 );

void recursivQuickSort(Array& toSort, int size)
{

	int pivot = toSort[0];
	Array& lowerArray = w->newArray(size);
	Array& greaterArray= w->newArray(size);
	int lowerSize = 0, greaterSize = 0; // effectives sizes
	// stop statement = condition + return (return stop the function even if it does not return anything)
	if ( size < 2) {
		return;
	}
	else {
		
		for (int i = 1; i<size; i++){
			int value = toSort[i];
			if (pivot>value) {
				lowerArray[lowerSize]=value;
				lowerSize++ ;
			}
			else {
				greaterArray[greaterSize] = value;
				greaterSize ++;
			}
			}
		}
	

		lowerArray[lowerSize] = pivot;
		lowerSize ++;

		recursivQuickSort(lowerArray, lowerSize);
		recursivQuickSort(greaterArray, greaterSize);

		
		toSort = fusion(lowerArray, greaterArray, lowerSize ,greaterSize);
		return;
}


	


Array fusion (Array& tab1, Array& tab2 , int size1 , int size2 ) {

	Array sorted(size1+size2);

	for (int i = 0; i<size1; i++){
		sorted[i]=tab1[i];
	}

	
	for (int j = size1; j<size1+size2; j++){
		sorted[j]=tab2[j-size1];
	}
	return sorted ; 
}

void quickSort(Array& toSort){
	recursivQuickSort(toSort, toSort.size());
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(quickSort);
	w->show();

	return a.exec();
}

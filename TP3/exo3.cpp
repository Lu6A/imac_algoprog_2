#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>
//#include <algorithm>

MainWindow* w = nullptr;
using std::size_t;

struct SearchTreeNode : public Node
{    
    SearchTreeNode* left;
    SearchTreeNode* right;
    int value;

    void initNode(int value)
    {
        // init initial node without children
        this->left = NULL ;
        this->right = NULL ;
        this->value = value ;

    }

	void insertNumber(int value) {
        // create a new node and insert it in right or left child


        if (value < this->value) 
        {

            if (this->left == NULL) 
            {
                this->left = new SearchTreeNode(value);
            }

            else 
            {
                this->left->insertNumber(value);
            }

        }

        else 
        {
            if (this->right == NULL)
            {
                this->right = new SearchTreeNode(value);
            }

            else
            {
                this->right->insertNumber(value);
            }
        }      

    }

	uint height() const	{



        if (this->right == NULL && this->left == NULL)
        {
            return 1;
        }
        
        int countLeft = 0;
        int countRight = 0;

        if (this->right != NULL)
        {
            countRight = this->right->height() ;
        }

        if (this->left != NULL)
        {
            countLeft = this->left->height() ;
        }
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        return std::max(countLeft,countRight) + 1;
    }

	uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        if (this->right == NULL && this->left == NULL)
        {
            return 1;
        }

        int nodesLeft = 0, nodesRight = 0;

        if (this->left != NULL)
        {
            nodesLeft = this->left->nodesCount();
        }
        if (this->right != NULL){
            nodesRight = this->right->nodesCount();
        }
        
        return nodesLeft + nodesRight + 1; 
	}

	bool isLeaf() const {
        if (this->right == NULL && this->left == NULL)
        {
            return true;
        }
        return false;
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        if (isLeaf())
        {
            leaves[leavesCount] = this;
            leavesCount ++;
        }
        if (this->left != NULL)
        {
            this->left->allLeaves(leaves,leavesCount);
        }
        if (this->right !=NULL)
        {
            this->right->allLeaves(leaves,leavesCount);
        }
	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {

            // fill nodes array with all nodes with inorder travel

    if (this->left != NULL)
    {
        this->left->inorderTravel(nodes, nodesCount);
    }

    nodes[nodesCount] = this;
    nodesCount ++;

    if (this->right != NULL)
    {
        this->right->inorderTravel(nodes, nodesCount);
    }
    
	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel

    nodes[nodesCount] = this;
    nodesCount ++;
    
    if (this->left != NULL)
    {
        this->left->preorderTravel(nodes,nodesCount);
    }

    if (this->right != NULL)
    {
        this->right->preorderTravel(nodes,nodesCount);
    }

	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel

    if (this->left != NULL)
    {
        this->left->postorderTravel(nodes,nodesCount);
    }
    if (this->right !=NULL)
    {
        this->right->postorderTravel(nodes,nodesCount);
    }

    nodes[nodesCount]=this;
    nodesCount ++;
	}

	Node* find(int value) {
        // find the node containing value
	if (this->value = value)
    {
        return this;
    }
    else if (value > this->value && this->right != NULL)
    {
        return this->right->find(value);
    }
    else if (value < this->value&& this->left !=NULL)
    {
        return this->left->find(value);
    }

    return NULL;
    }

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    SearchTreeNode(int value) : Node(value) {initNode(value);}
    ~SearchTreeNode() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new SearchTreeNode(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<SearchTreeNode>();
	w->show();

	return a.exec();
}

#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    Noeud* dernier;
    // your code
};

struct DynaTableau{
    int* elements;
    int nbElements;
    int MAX_ELEMENTS;
    // your code
};


void initialise(Liste* liste)
{
    liste->premier=NULL;
    liste->dernier=NULL;
}

bool est_vide(const Liste* liste)
{
    return liste->premier==NULL;
}

void ajoute(Liste* liste, int valeur)
{
    Noeud* noeud = new Noeud ; 
    noeud->donnee=valeur;
    if (est_vide(liste)){
        liste->premier=noeud;
        liste->dernier=noeud;
    }
    else{
        liste->dernier->suivant = noeud;
        liste->dernier = noeud;
    }
}

void affiche(const Liste* liste)
{
    if (est_vide(liste)) {
        cout << "La liste est vide" << endl;
    }
    Noeud* indice = liste->premier;
    while (indice != liste->dernier) {
        cout << indice->donnee << endl ; 
        indice = indice->suivant ;
    }
}

int recupere(const Liste* liste, int n)
{
    Noeud* indice = liste->premier ;
    int compteur = 0; 
    while (compteur != n || indice->suivant == NULL) {
        indice = indice->suivant;
        compteur ++ ;
    }
    if (indice->suivant == NULL){
        cout << "la liste est trop courte" << endl ;
        return -1;
    }
    return (indice->donnee);
}


int cherche(const Liste* liste, int valeur)
{
    Noeud* indice = liste->premier;
    int count ; 
    while (indice->suivant != NULL){
        if (indice->donnee==valeur){
            return count ;
        }
        else {
            indice = indice->suivant;
            count ++ ;
        }
    }
    return -1;
}

void stocke(Liste* liste, int n, int valeur)
{
    int count = 0 ;
    Noeud* indice = liste->premier ;
    while ((count!=n) | (indice->suivant != NULL)) {
        indice = indice->suivant ;
        count ++ ; 
    }
    if (indice->suivant ==NULL) {
        cout << "la liste est trop courte" << endl ;
    }

    else {
        indice->donnee = valeur ;
    }
}

void ajoute(DynaTableau* tableau, int valeur)
{
    if (tableau->nbElements<tableau->MAX_ELEMENTS) {
        tableau->elements[tableau->nbElements]=valeur ;
        tableau->nbElements ++ ;

    }
    else {
        tableau->MAX_ELEMENTS +=1 ;
        int* newmemo = (int*)realloc(tableau->elements,tableau->nbElements*sizeof(int));
        if (newmemo == NULL) {
            cout << "Il n'y a pas assez de mémoire" <<endl ;
        }
        else {
            tableau->elements = newmemo ;
            tableau->elements[tableau->nbElements] = valeur ;
            tableau->nbElements += 1 ;
        }
    }
    
}


void initialise(DynaTableau* tableau, int capacite)
{
    tableau->MAX_ELEMENTS = capacite ;
    tableau->nbElements = 0 ;
    tableau->elements = (int*)malloc(sizeof(int)*capacite);
}

bool est_vide(const DynaTableau* liste)
{
    return liste->nbElements == 0;
}

void affiche(const DynaTableau* tableau)
{
    for (int i=0 ; i < tableau->nbElements ; i++ ) {
        cout << tableau->elements[i] << endl;
    }

}

int recupere(const DynaTableau* tableau, int n)
{
    if (n>tableau->nbElements) {
        cout << "le tableau n'est pas assez grand"<<endl ;
        return -1 ;
    }
    else {
        return tableau->elements[n] ;
    }
}

int cherche(const DynaTableau* tableau, int valeur)
{
    for (int i = 0; i<tableau->nbElements; i++) {
        if (tableau->elements[i] == valeur) {
            return i ;
        }
    }
    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    if (n>tableau->nbElements) {
        cout << "le tableau n'est pas assez grand" <<endl;
    }
    else {
        tableau->elements[n] = valeur ;
    }
}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    ajoute(liste,valeur);
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
    Noeud* first = liste->premier ;
    liste->premier = liste->premier->suivant ;
    return first->donnee ;

}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur)
{
    ajoute (liste,valeur);
}

//int retire_pile(DynaTableau* liste)
int retire_pile(DynaTableau* liste)
{
    int retourne = liste->elements [liste->nbElements] ;
    int* supprime = &(liste->elements [liste->nbElements]);
    supprime = NULL ;
    liste->nbElements -- ;
    
    return retourne ; 
}

int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&liste) && compteur > 0)
    {
        std::cout << retire_file(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_file(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
